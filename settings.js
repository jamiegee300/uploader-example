module.exports = {
    postgres: {
        user: process.env.DB_USER || 'files',
        host: process.env.DB_HOST || 'localhost',
        database: process.env.DB_DATABASE || 'files_db',
        password: process.env.DB_PASSWORD || '',
        port: process.env.DB_PORT || 5432,
    }
};