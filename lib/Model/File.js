"use strict";

class File {

    constructor(properties) {
        this.path = properties.path;
        this.name = properties.name;
        this.data = properties.data;
    }

    setData(data) {
        this.data = '\\x' + data;
    }

}

module.exports = File;