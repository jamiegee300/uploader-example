"use strict";
const { Client } = require('pg');

class Postgres {

    constructor(settings, cl) {
        this.client = (!cl) ? new Client(settings.postgres) : cl;
        this.client.connect(err => {
            if (err)
                console.log("Error connecting to db ", err);

            this.client.query('CREATE TABLE IF NOT EXISTS file (id integer, name varchar, data varchar)', [], err => {
                if (err)
                    console.log("Error creating file table ", err);
            });
        });
    }

    insert(table, item, done) {
        const queryToExecute = 'INSERT INTO ' + table + '(name, data) ' +
                               'VALUES($1, $2) ' +
                               'RETURNING id';

        this.client.query(queryToExecute, [ item.name, item.data ], err => {
            if (err) return done(err);

            done();
        });
    }

    select(table, name, done) {
        const queryToExecute = 'SELECT name, data ' +
                               'FROM ' + table + ' ' +
                               'WHERE name = "' + name + '" ' +
                               'LIMIT 1';

        this.client.query(queryToExecute, (err, result) => {
            if (err) return done(err);

            done(null, result);
        });
    }

}

module.exports = Postgres;