"use strict";
const debug = require('debug')('uploader:service:fileService');
const fs = require('fs');
const path = require('path');
const _ = require('lodash');
const Model = require('../Model');

class FilesService {
    constructor(dependencies, _fs) {
        this.db = dependencies.db;
        this.fs = (!_fs) ? fs : _fs;
    }

    map(files, values) {
        return new Promise(resolve => {
            resolve(_.map(files, file => {
                return new Model.File({
                    name: values[file.fieldName + '_label'] + path.extname(file.path),
                    path: file.path
                });
            }));
        });
    }

    save(files) {
        const filesToSave = files.map(file => {
            return new Promise((resolve, reject) => {
                this.fs.readFile(file.path, 'hex', (err, data) => {
                    if (err) return reject(err);
                    if (!data) return resolve(null);

                    file.setData(data);
                    this.db.insert('file', file, err => {
                        if (err) return reject(err);

                        resolve(file);
                    });
                });
            });
        });

        return Promise.all(filesToSave);
    }

    removeTemporaryFiles(files) {
        return Promise.all(files.map(file => {
            return new Promise(resolve => {
                fs.unlink(file.path, (err) => {
                    if (err) debug('Error removing temporary file ', err);

                    resolve();
                });
            });
        }));
    }

    load(name) {
        return new Promise((resolve, reject) => {
            this.db.select('file', name, (err, result) => {
                if (err) return reject(err);

                const path = '/tmp/' + name;
                const data = result.rows[0].data;

                this.fs.writeFile(path, data, (err) => {
                    if (err) return reject(err);

                    resolve(new Model.File({ name, path, data }));
                });
            });
        });
    }
}

module.exports = FilesService;