const expect = require('expect.js');
const sinon = require('sinon');
const Model = require('../../../lib/Model');

const FileService = require('../../../lib/services/FilesService');

const stubs = {
    db: {
        insert: sinon.stub().yields(null)
    }
};
const fileContent = "obewfeiwbnfewewofmewpofmnew";
const encodedFileContent = '\\x' + fileContent;
const fs = {
    readFile: sinon.stub().yields(null, fileContent),
    writeFile: sinon.stub().yields(null)
};

describe('Given I have files and a file service', function () {

    const fileService = new FileService(stubs, fs);

    describe('When I call the file service save method passing the files', function () {

        it('Should read the temporary file from disk', function (done) {
            const files = [
                new Model.File({ path: '1', name: "one" }),
                new Model.File({ path: '2', name: "two" }),
                new Model.File({ path: '3', name: "three" })
            ];

            fileService.save(files).then(() => {
                expect(fs.readFile.callCount).to.eql(3);
                expect(fs.readFile.calledWith(files[0].path)).to.eql(true);
                expect(fs.readFile.calledWith(files[1].path)).to.eql(true);
                expect(fs.readFile.calledWith(files[2].path)).to.eql(true);

                done();
            }).catch(err => {
                console.log('ERROR - ', err);
            });
        });

        it('Should execute a command to insert the encoded data into a database', function (done) {
            const files = [
                new Model.File({ path: '1', name: "one" }),
                new Model.File({ path: '2', name: "two" }),
                new Model.File({ path: '3', name: "three" })
            ];

            fileService.save(files).then(() => {
                expect(stubs.db.insert.calledWith('file', { data: encodedFileContent, name: files[0].name, path: files[0].path })).to.eql(true);
                expect(stubs.db.insert.calledWith('file', { data: encodedFileContent, name: files[1].name, path: files[1].path })).to.eql(true);
                expect(stubs.db.insert.calledWith('file', { data: encodedFileContent, name: files[2].name, path: files[2].path })).to.eql(true);

                done();
            }).catch(err => {
                console.log('ERROR - ', err);
            });
        });

    });

});