const expect = require('expect.js');
const sinon = require('sinon');

const Database = require('../../lib/Postgres');
const settings = { postgres: {} };
const client = {
    connect: sinon.stub().yields(null),
    query: sinon.stub().yields(null)
};

describe('Given I have a database client', function () {

    describe('when instantiating', function () {

        const database = new Database(settings, client);

        it('should connect', function (done) {
            expect(client.connect.called).to.eql(true);
            done();
        });

        it('should call a query to ensure the file table exists', function (done) {
            expect(client.query.calledWith('CREATE TABLE IF NOT EXISTS file (id integer, name varchar, data varchar)', [])).to.eql(true);
            done();
        });

    });

});

describe('Given I have a database client', function () {

    const database = new Database(settings, client);

    describe('when calling insert passing in a table name and a record', function () {

        const table = "file";
        const record = { data: "data here", name: "my record" };

        it('should call client query passing in the correct query text and parameters', function (done) {
            database.insert(table, record, function (err) {
                if (err) console.log("Error - ", err);

                expect(client.query.calledWith('INSERT INTO file(name, data) VALUES($1, $2) RETURNING id', [ record.name, record.data ])).to.eql(true);
                done();
            });
        });


    });

});