const expect = require('expect.js');
const Model = require('../../../lib/Model');

describe('Given I have details of a file', function () {

    const path = "/path/to/file.jpg";
    const name = "image.jpg";
    const data = "oiwenfmnf524ewmfp23r235f2oewnfnfnfmf";

    describe('when I create a file', function () {

        const file = new Model.File({
            path,
            name,
            data
        });

        it('should have the correct path', function () {
            expect(file.path).to.eql(path);
        });

        it('should have the correct name', function () {
            expect(file.name).to.eql(name);
        });

        it('should have the correct data', function () {
            expect(file.data).to.eql(data);
        });

    });

});

describe('Given I have a file', function () {

    const path = "/path/to/file.jpg";
    const name = "image.jpg";
    const data = "";
    const newData = "piwnefpiwenfn";

    const file = new Model.File({
        path,
        name,
        data
    });

    describe('when calling setData', function () {

        file.setData(newData);

        it('should set the data property on the file to \\x plus the value passed in', function () {
            expect(file.data).to.eql("\\x" + newData);
        });

    });

});