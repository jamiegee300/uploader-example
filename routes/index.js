const debug = require('debug')('uploader:routes');
const express = require('express');
const router = express.Router();
const multipart = require('connect-multiparty');
const multipartMiddleware = multipart();

module.exports = function (dependencies) {
    const filesService = dependencies.filesService;

    router.get('/', function(req, res, next) {
        res.render('index', { title: 'Upload your files' });
    });

    router.post('/files', multipartMiddleware, function(req, res, next) {
        filesService.map(req.files, req.body)
                    .then(result => { return filesService.save(result); })
                    .then(result => { return filesService.removeTemporaryFiles(result); })
                    .then(() => {
                        res.render('thanks', { title: 'Upload your files' });
                    })
                    .catch(err => {
                       debug('Error saving files - ', err);

                       res.render('error');
                    });
    });

    // TODO: Add route to get image.

    return router;
};
