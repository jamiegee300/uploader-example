$.fn.multiuploader = function(options) {
    var settings = $.extend({}, options);
    var self = this;

    this.attr({ "enctype": "multipart/form-data" });
    this.append('<div class="form-group">' +
                            '<input id="fileField0" class="form-control-file btn" type="file" name="file0" />' +
                            '<input id="fileLabel0" class="form-control" type="text" name="file0_label" placeholder="...name" />' +
                        '</div>' +
                        '<div class="form-group">' +
                            '<button class="btn btn-primary float-right" type="submit">Submit</button>' +
                            '<button id="addAnotherFileButton" class="btn btn-default float-right" type="button">Add Another File</button>' +
                        '</div>');

    $(this).find("#addAnotherFileButton").click(function () {
        var newFileFieldNumber = $(self).find('input[type="file"]').length;

        $('<div class=\"form-group\">' +
            '<input id=\"fileField' + newFileFieldNumber + '\" class=\"form-control-file btn\" type=\"file\" name=\"file' + newFileFieldNumber + '\" />' +
            '<input id=\"fileLabel' + newFileFieldNumber + '\" class=\"form-control\" type=\"text\" name=\"file' + newFileFieldNumber + '_label\" placeholder=\"...name\" />' +
            '</div>').insertBefore($(this).parent());
    });

    return this;
};

$(document).ready(function () {

    $('#uploader').multiuploader();

});